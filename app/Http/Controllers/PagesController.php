<?php

namespace App\Http\Controllers;

class PagesController extends Controller
{
    public function stream()
    {
        return view('stream');
    }

    public function subscribe()
    {
        return view('subscribe');
    }

    public function profile()
    {
        return view('profile');
    }

    public function live()
    {
        return view('live');
    }

    public function help()
    {
        return 'Detailed help and documentation page yet to follow';
    }

    public function settings()
    {
        return 'Settings page yet to follow';
    }
}
