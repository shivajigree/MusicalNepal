<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Musical Nepal</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700">
  
    <link rel="stylesheet" href="css/homepage/Features-Boxed.css"> 
    <link rel="stylesheet" href="css/homepage/Footer-Basic.css">
    <link rel="stylesheet" href="css/homepage/Header-Blue.css">
    <link rel="stylesheet" href="css/homepage/Navigation-Clean.css">
    
</head>


<body>

    <div style="height:117px;">
        <div class="header-blue" style="padding-bottom:0;">
            <nav class="navbar navbar-dark navbar-expand-md navigation-clean-search" style="color:#ffffff;">
                <div class="container"><a class="navbar-brand" href="#" style="font-size:51px;">Musical Nepal</a><button class="navbar-toggler" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>

                    @if (Route::has('login'))
                    <div
                        class="collapse navbar-collapse" id="navcol-1">
                        <ul class="nav navbar-nav"></ul>

                    @auth
                        <a class="btn btn-light ml-auto action-button" role="button" href="{{ url('/home') }}" style="background-color:#009e60;margin:9px;">Home</a>
                    @else
                        <a class="btn btn-light ml-auto action-button" role="button" href="{{ route('login') }}" style="background-color:#009e60;margin:9px;">LOGIN</a>
                        <a class="btn btn-light action-button" role="button" href="{{ route('register') }}" style="background-color:#0072c6;margin:-14px;">REGISTER</a></div>
                    @endauth
        </div>
        @endif
        </nav>

    </div>
    </div>
    <div class="d-block" data-bs-parallax-bg="true" style="background-image:url('Music.jpg');background-position:center;background-size:cover;height:660px;"></div>
    <div style="background-color:rgb(51,45,49);padding:2px;"></div>
    <div class="features-boxed" style="background-image:url('ripple_sunset_4k-1920x1080.jpg');background-size:auto;background-position:center;">
        <div class="container">
            <div class="intro">
                <h2 class="text-center" style="color:rgb(255,255,255);">FEATURES</h2>
            </div>
            <div class="row justify-content-center features">
                <div class="col-sm-6 col-md-5 col-lg-4 item">
                    <div class="box" style="background-color:transparent;"><i class="fa fa-camera-retro icon" style="color:rgb(102,225,91);"></i>
                        <h3 class="name" style="color:rgb(255,255,255);">Video Streaming</h3>
                        <p class="description">Real time video streaming through your device.</p><a href="#" class="learn-more">Learn more »</a></div>
                </div>
                <div class="col-sm-6 col-md-5 col-lg-4 item"></div>
                <div class="col-sm-6 col-md-5 col-lg-4 item">
                    <div class="box" style="background-color:transparent;"><i class="fa fa-trophy icon" style="color:rgb(232,225,59);"></i>
                        <h3 class="name" style="color:rgb(255,255,255);">Rewards System</h3>
                        <p class="description">Maximum Engagement gives you maximum benefits.</p><a href="#" class="learn-more">Learn more »</a></div>
                </div>
                <div class="col-sm-6 col-md-5 col-lg-4 item">
                    <div class="box" style="background-color:transparent;"><i class="fa fa-map-marker icon" style="color:rgb(238,20,20);"></i>
                        <h3 class="name" style="color:rgb(255,255,255);">Location Based Filters</h3>
                        <p class="description">Surf to variouos places with our intelligent filters that can give you exact location.</p><a href="#" class="learn-more">Learn more »</a></div>
                </div>
                <div class="col-sm-6 col-md-5 col-lg-4 item"></div>
                <div class="col-sm-6 col-md-5 col-lg-4 item">
                    <div class="box" style="background-color:transparent;"><i class="fa fa-bell icon"></i>
                        <h3 class="name" style="color:rgb(255,255,255);">Notifications &amp; Recommendations</h3>
                        <p class="description">Never miss a thing with our sophisticated notify and recommend system.</p><a href="#" class="learn-more">Learn more »</a></div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-basic" style="color:rgb(24,78,142);">
        <footer>
            
            <ul class="list-inline">
                <li class="list-inline-item"><a href="#">Home</a></li>
                <li class="list-inline-item"><a href="#">Services</a></li>
                <li class="list-inline-item"><a href="#">About</a></li>
                <li class="list-inline-item"><a href="#">Terms</a></li>
                <li class="list-inline-item"><a href="#">Privacy Policy</a></li>
            </ul>
            <p class="copyright">Musical Nepal© 2018<br></p>
            <p class="copyright" style="margin-top:-2px;">GCES Major Project</p>
        </footer>
    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js"></script>
    <script src="js/bs-animation.js"></script>
    
</body>

</html>