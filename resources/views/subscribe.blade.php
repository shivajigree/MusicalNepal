<!DOCTYPE html>
<html>
<head>
	<title>WEBRTC</title>
	 <meta charset="utf-8">
    <title>Musical Nepal</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Musical Nepal, an online music streaming site.">
    <meta name="author" content="Shiva Kunwar">

	
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link id="bs-css" href="css/bootstrap-cerulean.min.css" rel="stylesheet">

    <link href="css/capp.css" rel="stylesheet">
    <link href='css/jquery.noty.css' rel='stylesheet'>
    <link href='css/noty_theme_default.css' rel='stylesheet'>
    <link href='css/elfinder.min.css' rel='stylesheet'>
    <link href='css/elfinder.theme.css' rel='stylesheet'>
    <link href='css/jquery.iphone.toggle.css' rel='stylesheet'>
    <link href='css/uploadify.css' rel='stylesheet'>
    <link href='css/animate.min.css' rel='stylesheet'>


    <!-- jQuery -->
    <script src="bower_components/jquery/jquery.min.js"></script>

  

    <!-- The fav icon -->
    <link rel="shortcut icon" href="img/favicon.ico">

    <!-- broadcaster -->
     <script type="text/javascript" src="js/swfobject.js"></script>
        <script type="text/javascript">
        swfobject.embedSWF("simpleSubscriber.swf", "myContent", "400", "400", "8.0.0", "js/expressInstall.swf");
        </script>


</head>
<body>
   <div class="alert alert-info">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong>Hello {{ Auth::user()->name }} !</strong> Your email id is {{ Auth::user()->email}}
    </div>

    <div id="ch-container">
        <div class="row"></div>
        <!-- left menu starts -->
        <div class="col-sm-2 col-lg-2">
            <div class="sidebar-nav">
                <div class="nav-canvas">
                    <div class="nav-sm nav nav-stacked">

                    </div>
                    <ul class="nav nav-pills nav-stacked main-menu">
                        <li class="nav-header">Main</li>
                        <li><a class="ajax-link" href="/home"><i class="glyphicon glyphicon-home"></i><span> Dashboard</span></a>
                        </li>
                        <li><a class="ajax-link" href="/profile"><i class="glyphicon glyphicon-picture"></i><span> Profile</span></a>
                        </li>
                        <li><a class="ajax-link" href="/stream"><i class="glyphicon glyphicon-camera"></i><span>Stream Page</span></a>
                        </li>
                        <li><a class="ajax-link" href="/subscribe"><i class="glyphicon glyphicon-camera"></i><span>Subscribe Page</span></a>
                    </li>
                    </ul>
                </div>
            </div>
        </div>

        <!-- left menu ends -->
        <noscript>
            <div class="alert alert-block col-md-12">
                <h4 class="alert-heading">Warning!</h4>

                <p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a>
                    enabled to use this site.</p>
            </div>
        </noscript>

        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
                <ul class="breadcrumb">
                    <li>
                        <a href="{{ url('/') }}">Home</a>
                    </li>
                    <li>
                        <a href="stream">Stream Page</a>
                    </li>
                </ul>
            </div>
        </div>
<!-- Live streaming section starts -->
<div class="row">
<section id="splash">
  <p id="errorMessage">Loading...</p>
</section>
<div id="myContent">
            <h1>You need the Adobe Flash Player for this demo</h1>
        </div>

</div>

<!-- Live streaming section ends -->
 

</div>
</div>
</body>
<!-- external javascript -->
<script src="https://webrtc.github.io/adapter/adapter-latest.js"></script>
<script src="{{ asset('js/app.js') }}"></script>
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- library for cookie management -->
<script src="js/jquery.cookie.js"></script>
<!-- calender plugin -->
<script src='bower_components/moment/min/moment.min.js'></script>
<script src='bower_components/fullcalendar/dist/fullcalendar.min.js'></script>
<!-- data table plugin -->
<script src='js/jquery.dataTables.min.js'></script>

<!-- select or dropdown enhancer -->
<script src="bower_components/chosen/chosen.jquery.min.js"></script>
<!-- plugin for gallery image view -->
<script src="bower_components/colorbox/jquery.colorbox-min.js"></script>
<!-- notification plugin -->
<script src="js/jquery.noty.js"></script>
<!-- library for making tables responsive -->
<script src="bower_components/responsive-tables/responsive-tables.js"></script>
<!-- tour plugin -->
<script src="bower_components/bootstrap-tour/build/js/bootstrap-tour.min.js"></script>
<!-- star rating plugin -->
<script src="js/jquery.raty.min.js"></script>
<!-- for iOS style toggle switch -->
<script src="js/jquery.iphone.toggle.js"></script>
<!-- autogrowing textarea plugin -->
<script src="js/jquery.autogrow-textarea.js"></script>
<!-- multiple file upload plugin -->
<script src="js/jquery.uploadify-3.1.min.js"></script>
<!-- history.js for cross-browser state change on ajax -->
<script src="js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<script src="js/charisma.js"></script>

</html>