<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
   <meta charset="utf-8">
   <title>Musical Nepal</title>
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <meta name="description" content="Musical Nepal, an online music streaming site.">
   <meta name="author" content="Shiva Kunwar">

   <!-- CSRF Token -->
   <meta name="csrf-token" content="{{ csrf_token() }}">

   <title>{{ config('app.name', 'Laravel') }}</title>

   <!-- Styles -->
   <link href="{{ asset('css/app.css') }}" rel="stylesheet">
   <link id="bs-css" href="css/bootstrap-cerulean.min.css" rel="stylesheet">
   <link href="css/capp.css" rel="stylesheet">
   <link href='css/jquery.noty.css' rel='stylesheet'>
   <link href='css/noty_theme_default.css' rel='stylesheet'>
   <link href='css/elfinder.min.css' rel='stylesheet'>
   <link href='css/elfinder.theme.css' rel='stylesheet'>
   <link href='css/jquery.iphone.toggle.css' rel='stylesheet'>
   <link href='css/uploadify.css' rel='stylesheet'>
   <link href='css/animate.min.css' rel='stylesheet'>


   <!-- jQuery -->
   <script src="bower_components/jquery/jquery.min.js"></script>

   @yield('css')

   <!-- The fav icon -->
   <link rel="shortcut icon" href="img/favicon.ico">

</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    
                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        <img alt="Musical Nepal Logo" src="img/logo.png" class="hidden-xs"/>
                        <span> {{ config('app.name', 'Musical Nepal') }}</span>

                    </a>
                    
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @guest
                        <li><a href="{{ route('login') }}">Login</a></li>
                        <li><a href="{{ route('register') }}">Register</a></li>
                        @else
                        <!-- user dropdown starts -->
                        <div class="btn-group pull-right">
                            <button style="margin: 5px;" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-user"></i><span class="hidden-sm hidden-xs"> {{ Auth::user()->name }}</span>
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <li><a href="/help">Help</a></li>
                                <li class="divider"></li>
                                <li><a href="/settings">Settings</a></li>
                                <li class="divider"></li>
                                <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">Logout</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form></li>
                            </ul>
                        </div>
                        <!-- user dropdown ends -->

                        <!-- theme selector starts -->
                        <div class="btn-group pull-right theme-container animated tada">
                            <button style="margin: 5px;" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-tint"></i><span
                                class="hidden-sm hidden-xs"> Change Theme</span>
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" id="themes">
                                <li><a data-value="classic" href="#"><i class="whitespace"></i> Classic</a></li>
                                <li><a data-value="cerulean" href="#"><i class="whitespace"></i> Cerulean</a></li>
                                <li><a data-value="cyborg" href="#"><i class="whitespace"></i> Cyborg</a></li>
                                <li><a data-value="simplex" href="#"><i class="whitespace"></i> Simplex</a></li>
                                <li><a data-value="darkly" href="#"><i class="whitespace"></i> Darkly</a></li>
                                <li><a data-value="lumen" href="#"><i class="whitespace"></i> Lumen</a></li>
                                <li><a data-value="slate" href="#"><i class="whitespace"></i> Slate</a></li>
                                <li><a data-value="spacelab" href="#"><i class="whitespace"></i> Spacelab</a></li>
                                <li><a data-value="united" href="#"><i class="whitespace"></i> United</a></li>
                            </ul>
                        </div>
                        <!-- theme selector ends -->
                        <!-- GO Live starts -->
                        <div class="btn-group pull-right theme-container"></div>
                        <div class="btn-group pull-right theme-container"></div>
                        <div class="btn-group pull-right theme-container"></div>
                        <div class="btn-group pull-right ">
                            <button style="margin: 5px;" class="btn btn-danger " >
                                <a href="/live">
                                    <i class="glyphicon glyphicon-facetime-video" style="color: #FFF"></i><span style="color: #FFF"> GO LIVE</span>
                                </a>
                            </button>
                        </div>
                        <!-- GO Live ends -->


                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')
    </div>


    <footer class="row">
        <p class="col-md-9 col-sm-9 col-xs-12 copyright">&copy; <a href="{{ url('/') }}" target="_blank">Musical Nepal</a> | Major Project by GCES 2014 Batch</p>


    </footer>

    <!-- external javascript -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- library for cookie management -->
    <script src="js/jquery.cookie.js"></script>
    <!-- calender plugin -->
    <script src='bower_components/moment/min/moment.min.js'></script>
    <script src='bower_components/fullcalendar/dist/fullcalendar.min.js'></script>
    <!-- data table plugin -->
    <script src='js/jquery.dataTables.min.js'></script>

    <!-- select or dropdown enhancer -->
    <script src="bower_components/chosen/chosen.jquery.min.js"></script>
    <!-- plugin for gallery image view -->
    <script src="bower_components/colorbox/jquery.colorbox-min.js"></script>
    <!-- notification plugin -->
    <script src="js/jquery.noty.js"></script>
    <!-- library for making tables responsive -->
    <script src="bower_components/responsive-tables/responsive-tables.js"></script>
    <!-- tour plugin -->
    <script src="bower_components/bootstrap-tour/build/js/bootstrap-tour.min.js"></script>
    <!-- star rating plugin -->
    <script src="js/jquery.raty.min.js"></script>
    <!-- for iOS style toggle switch -->
    <script src="js/jquery.iphone.toggle.js"></script>
    <!-- autogrowing textarea plugin -->
    <script src="js/jquery.autogrow-textarea.js"></script>
    <!-- multiple file upload plugin -->
    <script src="js/jquery.uploadify-3.1.min.js"></script>
    <!-- history.js for cross-browser state change on ajax -->
    <script src="js/jquery.history.js"></script>
    <!-- application script for Charisma demo -->
    <script src="js/charisma.js"></script>


</body>
</html>