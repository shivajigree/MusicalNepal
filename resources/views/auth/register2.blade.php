
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Musical Nepal | Register</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700">
 
    <link rel="stylesheet" href="css/homepage/Register-Form-Dark.css">
    <link rel="stylesheet" href="css/homepage/Footer-Basic.css">


<body>
    <div class="d-flex login-dark" style="height:662px; background:#475d62 url(guitar.jpg);">
        <form class="form-inline" method="POST" action="{{ route('register') }}">
         {{ csrf_field() }}

            <div class="form-div" style="margin-left:177px;height:381px;">
                <h1 style="font-size:24px;">Addon Details</h1>

<!-- Favorite artists begins -->
                <div class="form-group{{ $errors->has('artist') ? ' has-error' : '' }}">
                    <input class="form-control" id="artist" type="text" name="artists" placeholder="Favorite Artists" style="height:70px;margin-top:10px;" required>
                    @if ($errors->has('artist'))
                        <span class="help-block">
                             <strong>{{ $errors->first('artist') }}</strong>
                        </span>
                    @endif
                </div>

<!-- Genre Interested begins -->
                <div class="form-group{{ $errors->has('genre') ? ' has-error' : '' }}">
                    <input class="form-control" id="gerne" type="text" name="genre" placeholder="Genre Interested" style="height:70px;" required>
                    @if ($errors->has('genre'))
                        <span class="help-block">
                             <strong>{{ $errors->first('genre') }}</strong>
                        </span>
                    @endif
                </div>

<!-- Bio begins -->
                <div class="form-group{{ $errors->has('bio') ? ' has-error' : '' }}">
                    <input class="form-control" id="bio" type="text" name="bio" placeholder="Bio (max. 200 words)" style="height:70px;" required>
                    @if ($errors->has('bio'))
                        <span class="help-block">
                             <strong>{{ $errors->first('bio') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        
            <div class="form-div" style="margin-left:-150px;height:381px;">
                <h1 class="text-center" style="font-size:24px;padding-bottom: 25px">Personal Details
                </h1>

<!-- Username begins   -->
                <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                    <input class="form-control" id="username" type="text" name="username" placeholder="Username" pattern="^[a-z0-9_-]{3,15}$" required>
                    @if ($errors->has('username'))
                        <span class="help-block">
                             <strong>{{ $errors->first('username') }}</strong>
                        </span>
                    @endif
                </div>

<!-- Full name begins -->
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <input class="form-control" id="name" type="text" name="name" placeholder="Full Name" required>
                    @if ($errors->has('name'))
                        <span class="help-block">
                             <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>

<!-- Address begins -->
                <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                    <input class="form-control" id="address" type="text" name="address" placeholder="Address" required>
                    @if ($errors->has('address'))
                        <span class="help-block">
                             <strong>{{ $errors->first('address') }}</strong>
                        </span>
                    @endif
                </div>                

<!-- Email begins -->
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <input class="form-control" id="email" type="email" name="email" placeholder="Email" pattern="^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" required>
                    @if ($errors->has('email'))
                        <span class="help-block">
                             <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>

<!-- Password begins -->
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <input class="form-control" id="password" type="password" name="password" placeholder="Password" required>
                    @if ($errors->has('password'))
                        <span class="help-block">
                             <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>

<!-- Repassword begins -->
                <div class="form-group">
                    <input class="form-control" id="password-confirm" type="password" name="password_confirmation" placeholder="Repeat Password" required>
                    @if ($errors->has('repassword'))
                        <span class="help-block">
                             <strong>{{ $errors->first('repassword') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

<!-- Register button for submissing form -->
        <div class="form-div" style="height:0;width:0;margin-left:-82px;margin-top:203px;padding:0;">
            <button class="btn btn-info btn-block" type="submit" style="width:195px;height:63px;padding:8px;">Register</button></div>
    </form>

    </div>

    <div class="footer-basic" style="color:rgb(24,78,142);">
        <footer>
            <p class="copyright">Musical Nepal© 2018<br></p>
            <p class="copyright" style="margin-top:-2px;">GCES Major Project</p>
        </footer>
    </div>
   <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js"></script>
    <script src="js/bs-animation.js"></script>
</body>

</html>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Musical Nepal | Register</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700">
 
    <link rel="stylesheet" href="css/homepage/Register-Form-Dark.css">
    <link rel="stylesheet" href="css/homepage/Footer-Basic.css">

<body>
    <div class="d-flex login-dark" style="height:1001px; backgroud:#475d62 url(guitar.jpg);">
        <form class="from-inline" method="post" action="{{ route('register') }}" style="width:598px;height:822px;padding-top:0px;padding-right:16px;padding-left:23px;min-width:47px;max-width:393px;">
            
            <div class="illustration"><i class="icon ion-ios-locked-outline"></i></div>
            <div class="form-group"><input class="form-control" type="text" name="name" required="" placeholder="Full Name"></div>
            <div class="form-group"><input class="form-control" type="text" name="username" required="" placeholder="Username" pattern="^[a-z0-9_-]{3,15}$"></div>
            <div class="form-group"><input class="form-control" type="email" name="email" required="" placeholder="Email" pattern="^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"></div>
            <div class="form-group"><input class="form-control" type="password" name="password" required="" placeholder="Password"></div>
            <div class="form-group"><input class="form-control" type="password" name="confirm-password" required="" placeholder="Confirm password"></div>
            <div class="form-group"><input class="form-control" type="text" name="address" required="" placeholder="Address"></div>
            <div class="form-group"><input class="form-control" type="text" name="genre" required="" placeholder="Genre Interested"></div>
            <div class="form-group">
                <div class="form-check"><input class="form-check-input" type="checkbox" name="terms" required="" id="formCheck-1" style="width:18px;height:18px;"><label class="form-check-label" for="formCheck-1" style="padding-left:9px;padding-top:1px;">I agree to the Terms and Privacy Policy&nbsp;<br></label></div>
            </div>
            <div class="form-group" style="margin-bottom:20px;margin-top:100px;"><button class="btn btn-primary btn-block" type="submit" style="height:44px;margin:3px;">Register</button></div><a href="#" class="forgot">Forgot your email or password?</a></form>
    </div>


    <div class="footer-basic" style="color:rgb(24,78,142);">
        <footer>
            <p class="copyright">Musical Nepal© 2018<br></p>
            <p class="copyright" style="margin-top:-2px;">GCES Major Project</p>
        </footer>
    </div>
   <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js"></script>
    <script src="js/bs-animation.js"></script>
</body>

</html>