<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Musial Nepal | Login</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700">

    <link rel="stylesheet" href="css/homepage/Login-Form-Dark.css">

</head>

<body>
    
    <div class="login-dark" style="height:653px; background:#475d62 url(moon.jpg);">
        <form method="post" action="{{ route('login') }}">
            <h2 class="sr-only">Login Form</h2>
            <div class="illustration"><i class="fa fa-play"></i></div>
            {{ csrf_field() }}

<!--Login username or email -->
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <p>Email / Username</p><input id="email" class="form-control" type="text" name="email" "{{ old('email') }}" required autofocus>
                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
            </div>
            
<!-- Login password -->            
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <p>Password</p><input id="password" type="password" class="form-control" name="password" required>
                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
            </div>
            
<!-- Remember Me -->
            <div class="form-group" style="height:22px;margin-left:65px;">
                <div class="form-check"><input class="form-check-input" type="checkbox" id="formCheck-1"><label class="form-check-label" for="formCheck-1" style="color:rgb(41,128,239);" name="remember" {{ old('remember') ? 'checked' : '' }}>Remember Me</label></div>
            </div>
            
<!-- Login button -->            
            <div class="form-group"><button class="btn btn-primary btn-block" type="submit">Log In</button></div>
            <a href="{{ route('password.request') }}" class="forgot">Forgot your email or password?</a>
            <a href="{{ route('register') }}" class="register">&lt;&lt;Register&gt;&gt;</a></form>
    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js"></script>
    <script src="js/bs-animation.js"></script>
</body>

</html>
