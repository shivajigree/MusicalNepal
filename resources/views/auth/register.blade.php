
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Musical Nepal | Register</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css">
    <link href="https://unpkg.com/ionicons@4.4.6/dist/css/ionicons.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700">
 
    <link rel="stylesheet" href="css/homepage/Features-Boxed.css">
    <link rel="stylesheet" href="css/homepage/Footer-Basic.css">
    <link rel="stylesheet" href="css/homepage/Header-Blue.css">
    <link rel="stylesheet" href="css/homepage/Register-Form-Dark.css">
  

   
</head>

<body>
    
    <div class="login-dark" style="height:1001px; background:url(regwall.jpg); background-repeat:no-repeat;">
        <form method="post" action="{{ route('register') }}" style="width:598px;height:822px;padding-top:0px;padding-right:16px;padding-left:23px;min-width:47px;max-width:393px;">
            {{ csrf_field() }}
                
            <div class="form-div"><h1 style="font-size: 24px; text-align:center; padding-top: 25px; ">Fill the form</h1></div>    

            <div class="illustration"><i class="icon ion-ios-musical-notes"></i></div>

<!-- Full name begins -->            
            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <input class="form-control" id="name" type="text" name="name" placeholder="Full Name" required>
                    @if ($errors->has('name'))
                        <span class="help-block">
                             <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>

<!-- Username begins -->            
            <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                    <input class="form-control" id="username" type="text" name="username" placeholder="Username" pattern="^[a-z0-9_-]{3,15}$" required>
                    @if ($errors->has('username'))
                        <span class="help-block">
                             <strong>{{ $errors->first('username') }}</strong>
                        </span>
                    @endif
                </div>

<!-- Email begins -->
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <input class="form-control" id="email" type="email" name="email" placeholder="Email">
                    @if ($errors->has('email'))
                        <span class="help-block">
                             <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>

<!-- Password begins -->
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <input class="form-control" id="password" type="password" name="password" placeholder="Password" required>
                    @if ($errors->has('password'))
                        <span class="help-block">
                             <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>

<!-- Confirm password begins -->
           <div class="form-group">
                    <input class="form-control" id="password-confirm" type="password" name="password_confirmation" placeholder="Repeat Password" required>
                    @if ($errors->has('repassword'))
                        <span class="help-block">
                             <strong>{{ $errors->first('repassword') }}</strong>
                        </span>
                    @endif
                </div>
            


<!-- Address begins -->
            <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                    <input class="form-control" id="address" type="text" name="address" placeholder="Address" required>
                    @if ($errors->has('address'))
                        <span class="help-block">
                             <strong>{{ $errors->first('address') }}</strong>
                        </span>
                    @endif
                </div>  

<!-- Genre Interested begins -->
            <div class="form-group{{ $errors->has('genre') ? ' has-error' : '' }}">
                    <input class="form-control" id="gerne" type="text" name="genre" placeholder="Genre Interested" style="height:70px;" required>
                    @if ($errors->has('genre'))
                        <span class="help-block">
                             <strong>{{ $errors->first('genre') }}</strong>
                        </span>
                    @endif
                </div>

<!-- Terms and Policy begins -->
            <div class="form-group">
                <div class="form-check"><input class="form-check-input" type="checkbox" name="terms" required="" id="formCheck-1" style="width:18px;height:18px;"><label class="form-check-label" for="formCheck-1" style="padding-left:9px;padding-top:1px;">I agree to the <a href="/terms">Terms</a> and <a href="/policy">Privacy Policy</a>&nbsp;<br></label></div>
            </div>

<!-- Register button for submission  -->
            <div class="form-group" style="margin-bottom:20px;"><button class="btn btn-info btn-block" type="submit" style="height:44px;margin:3px;">Register</button></div>
            <a href="/" class="forgot">Go to Homepage</a></form>
    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js"></script>
    <script src="js/bs-animation.js"></script>
</body>

</html>