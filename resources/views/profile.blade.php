@extends('layouts.app') 

@section('content')

<div class="alert alert-info">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <strong>Hello {{ Auth::user()->name }} !</strong> Your email id is {{ Auth::user()->email}}
</div>

<div id="ch-container">
    <div class="row"></div>
    <!-- left menu starts -->
    <div class="col-sm-2 col-lg-2"> 
        <div class="sidebar-nav">
            <div class="nav-canvas">
                <div class="nav-sm nav nav-stacked">

                </div>
                <ul class="nav nav-pills nav-stacked main-menu">
                    <li class="nav-header">Main</li>
                    <li><a class="ajax-link" href="/home"><i class="glyphicon glyphicon-home"></i><span> Dashboard</span></a>
                    </li>
                    <li><a class="ajax-link" href="/profile"><i class="glyphicon glyphicon-picture"></i><span> Profile</span></a>
                    </li>
                    <li><a class="ajax-link" href="/stream"><i class="glyphicon glyphicon-camera"></i><span>Stream Page</span></a>
                    </li>
                    <li><a class="ajax-link" href="/subscribe"><i class="glyphicon glyphicon-camera"></i><span>Subscribe Page</span></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <!-- left menu ends -->

    <noscript>
            <div class="alert alert-block col-md-12">
                <h4 class="alert-heading">Warning!</h4>

                <p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a>
                    enabled to use this site.</p>
            </div>
        </noscript>

    <div id="content" class="col-lg-10 col-sm-10">
        <!-- content starts -->
        <div>
            <ul class="breadcrumb">
                <li>
                    <a href="{{ url('/') }}">Home</a>
                </li>
                <li>
                    <a href="profile">Profile</a>
                </li>
            </ul>
        </div>

        <!-- Profile section starts -->


        <div class="panel panel-info">
            <div class="panel-heading">
                <h4 class="panel-title">{{ Auth::user()->name }}</h4>
                <a href="#" class="btn text-right nav justify-content-end">Edit <i class="glyphicon glyphicon-edit"></i></a>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-3 col-lg-3 " align="center"> <img alt="User Pic" src="img/avatar.png" class="img-circle img-responsive"> </div>


                    <div class=" col-md-9 col-lg-9 ">
                        <table class="table table-user-information">
                            <tbody>
                                <tr>
                                    <td>Date of Birth</td>
                                    <td>01/24/1995</td>
                                </tr>
                                <tr>
                                    <td>Home Address</td>
                                    <td>{{ Auth::user()->address  }}</td>
                                </tr>
                                <tr>
                                    <td>Email</td>
                                    <td><a href="mailto:{{ Auth::user()->email }}">{{ Auth::user()->email }}</a></td>
                                </tr>
                                <tr>
                                    <td>Joined date</td>
                                    <td>{{ Auth::user()->created_at }}</td>
                                </tr>

                                <tr>
                                    <td>Phone Number</td>
                                    <td>555-4567-890(Mobile)
                                    </td>
                                </tr>

                                <tr>
                                    <td>Genre</td>
                                    <td><span class=" btn btn-info btn-xs">{{ Auth::user()->genre}}</span>
                                       
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection