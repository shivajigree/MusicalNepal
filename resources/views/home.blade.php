@extends('layouts.app') 

@section('content')
<div class="alert alert-info">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <strong>Hello {{ Auth::user()->name }} !</strong> Your email id is {{ Auth::user()->email}}

</div>

<div class="ch-container">
    <div class="row">

        <!-- left menu starts -->
        <div class="col-sm-2 col-lg-2">
            <div class="sidebar-nav">
                <div class="nav-canvas">
                    <div class="nav-sm nav nav-stacked">

                    </div>
                    <ul class="nav nav-pills nav-stacked main-menu">
                        <li class="nav-header">Main</li>
                        <li><a class="ajax-link" href="/home"><i class="glyphicon glyphicon-home"></i><span> Dashboard</span></a>
                        </li>
                        <li><a class="ajax-link" href="/profile"><i class="glyphicon glyphicon-picture"></i><span> Profile</span></a>
                        </li>
                        <li><a class="ajax-link" href="/stream"><i class="glyphicon glyphicon-camera"></i><span>Stream Page</span></a>
                        </li>
                        <li><a class="ajax-link" href="/subscribe"><i class="glyphicon glyphicon-camera"></i><span>Subscribe Page</span></a>
                    </li>
                    </ul>
                </div>
            </div>
        </div>

        <!-- left menu ends -->

        <noscript>
            <div class="alert alert-block col-md-12">
                <h4 class="alert-heading">Warning!</h4>

                <p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a>
                    enabled to use this site.</p>
            </div>
        </noscript>

        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
                <ul class="breadcrumb">
                    <li>
                        <a href="{{ url('/') }}">Home</a>
                    </li>
                    <li>
                        <a href="{{ route('home') }}">Dashboard</a>
                    </li>
                </ul>
            </div>


            <!-- Stats bar starts -->
            <div class=" row">
                <div class="col-md-3 col-sm-3 col-xs-6">
                    <a data-toggle="tooltip" title="6 new subscribers." class="well top-block" href="#">
            <i class="glyphicon glyphicon-user blue"></i>

            <div>Subscribers</div>
            <div>507</div>
            <span class="notification">6</span>
        </a>
                </div>

                <div class="col-md-3 col-sm-3 col-xs-6">
                    <a data-toggle="tooltip" title="4 likes in last live" class="well top-block" href="#">
            <i class="glyphicon glyphicon-thumbs-up green"></i>

            <div>Likes</div>
            <div>228</div>
            <span class="notification green">4</span>
        </a>
                </div>

                <div class="col-md-3 col-sm-3 col-xs-6">
                    <a data-toggle="tooltip" title="34 views in last live" class="well top-block" href="#">
            <i class="glyphicon glyphicon-shopping-cart yellow"></i>

            <div>Views</div>
            <div>2547</div>
            <span class="notification yellow">34</span>
        </a>
                </div>

                <div class="col-md-3 col-sm-3 col-xs-6">
                    <a data-toggle="tooltip" title="12 new messages." class="well top-block" href="#">
            <i class="glyphicon glyphicon-envelope red"></i>

            <div>Messages</div>
            <div>25</div>
            <span class="notification red">12</span>
        </a>
                </div>
            </div>
            <!-- Stats bar starts -->

            <!-- Notification bar starts -->
            <div class="row">
                <div class="box col-md-12">
                    <div class="box-inner">
                        <div class="box-header well">
                            <h2><i class="glyphicon glyphicon-info-sign"></i> New Notifications</h2>

                            
                        </div>
                        <div class="box-content row">
                            <div class="col-lg-7 col-md-12">
                                <h1>Like feature added <br>
                                    <small>free tokens to give for your favorite fans</small>
                                </h1>
                                <p>Now the performer can give free digital gifts and coupons to their favorite fans.<br/>It can be redemeed in Offers section :)
                                </p>

                                <p><b>Visit help section for more info.</b></p>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Notification bar starts -->

            <!--   -->
            <div class="row">

                <!--/span-->

                <div class="box col-md-4">
                    <div class="box-inner">
                        <div class="box-header well" data-original-title="">
                            <h2><i class="glyphicon glyphicon-user"></i> Recent Subscribers</h2>

                           
                        </div>
                        <div class="box-content">
                            <div class="box-content">
                                <ul class="dashboard-list">
                                    <li>
                                        <a href="#">
                                <img class="dashboard-avatar" alt="Usman"
                                     src="img\logo.png"></a>
                                        <strong>Name:</strong> <a href="#">Sudip
                            </a><br>
                                        <strong>Since:</strong> 17/05/2014<br>
                                        <strong>Status:</strong> <span class="label-success label label-default">Approved</span>
                                    </li>
                                    <li>
                                        <a href="#">
                                <img class="dashboard-avatar" alt="Sheikh Heera"
                                     src="img\logo.png"></a>
                                        <strong>Name:</strong> <a href="#">Sanjay
                            </a><br>
                                        <strong>Since:</strong> 17/05/2014<br>
                                        <strong>Status:</strong> <span class="label-warning label label-default">Pending</span>
                                    </li>
                                    <li>
                                        <a href="#">
                                <img class="dashboard-avatar" alt="Abdullah"
                                     src="img\logo.png"></a>
                                        <strong>Name:</strong> <a href="#">Rajesh
                            </a><br>
                                        <strong>Since:</strong> 25/05/2014<br>
                                        <strong>Status:</strong> <span class="label-default label label-danger">Banned</span>
                                    </li>
                                    <li>
                                        <a href="#">
                                <img class="dashboard-avatar" alt="Sana Amrin"
                                     src="img\logo.png"></a>
                                        <strong>Name:</strong> <a href="#">Amrit</a><br>
                                        <strong>Since:</strong> 17/05/2014<br>
                                        <strong>Status:</strong> <span class="label label-info">Updates</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/span-->

                <div class="box col-md-4">
                    <div class="box-inner">
                        <div class="box-header well" data-original-title="">
                            <h2><i class="glyphicon glyphicon-list"></i> Weekly Total Stat</h2>

                           
                        </div>
                        <div class="box-content">
                            <ul class="dashboard-list">
                                <li>
                                    <a href="#">
                            <i class="glyphicon glyphicon-arrow-up"></i>
                            <span class="green">92</span>
                            Subscribers  
                        </a>
                                </li>
                                <li>
                                    <a href="#">
                            <i class="glyphicon glyphicon-arrow-down"></i>
                            <span class="red">15</span>
                            Likes
                        </a>
                                </li>
                                <li>
                                    <a href="#">
                            <i class="glyphicon glyphicon-minus"></i>
                            <span class="blue">36</span>
                            Views
                        </a>
                                </li>
                                <li>
                                    <a href="#">
                            <i class="glyphicon glyphicon-comment"></i>
                            <span class="yellow">45</span>
                            Comments
                        </a>
                                </li>
                                <li>
                                    <a href="#">
                            <i class="glyphicon glyphicon-arrow-up"></i>
                            <span class="green">112</span>
                            Maximum views
                        </a>
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
            

            <!--
                
                <div class="box col-md-4">
                    <div class="box-inner">
                        <div class="box-header well" data-original-title="">
                            <h2><i class="glyphicon glyphicon-list"></i> Buttons</h2>
                           
                        </div>
                        <div class="box-content buttons">
                            <p class="btn-group">
                                <button class="btn btn-default">Left</button>
                                <button class="btn btn-default">Middle</button>
                                <button class="btn btn-default">Right</button>
                            </p>
                            <p>
                                <button class="btn btn-default btn-sm"><i class="glyphicon glyphicon-star"></i> Icon button</button>
                                <button class="btn btn-primary btn-sm">Small button</button>
                                <button class="btn btn-danger btn-sm">Small button</button>
                            </p>
                            <p>
                                <button class="btn btn-warning btn-sm">Small button</button>
                                <button class="btn btn-success btn-sm">Small button</button>
                                <button class="btn btn-info btn-sm">Small button</button>
                            </p>
                            <p>
                                <button class="btn btn-inverse btn-default btn-sm">Small button</button>
                                <button class="btn btn-primary btn-round btn-lg">Round button</button>
                                <button class="btn btn-round btn-default btn-lg"><i class="glyphicon glyphicon-ok"></i></button>
                                <button class="btn btn-primary"><i class="glyphicon glyphicon-edit glyphicon-white"></i></button>
                            </p>
                            <p>
                                <button class="btn btn-default btn-xs">Mini button</button>
                                <button class="btn btn-primary btn-xs">Mini button</button>
                                <button class="btn btn-danger btn-xs">Mini button</button>
                                <button class="btn btn-warning btn-xs">Mini button</button>
                            </p>
                            <p>
                                <button class="btn btn-info btn-xs">Mini button</button>
                                <button class="btn btn-success btn-xs">Mini button</button>
                                <button class="btn btn-inverse btn-default btn-xs">Mini button</button>
                            </p>
                        </div>
                    </div>
            </div>
                /span-->
            </div>
        </div>

 @endsection