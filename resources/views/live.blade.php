@extends('layouts.app') 

@section('css')
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha256-3dkvEK0WLHRJ7/Csr0BZjAWxERc5WH7bdeUya2aXxdU= sha512-+L4yy6FRcDGbXJ9mPG8MT/3UCDzwR9gPeyFNMCtInsol++5m3bk2bXWKdZjvybmohrAsn3Ua5x8gfLnbE1YkOg==" crossorigin="anonymous">
    <!-- Bootstrap Core CSS -->
<!--     <link href="css/bootstrap.min.css" rel="stylesheet"> -->
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" integrity="sha256-7s5uDGW3AHqw6xtJmNNtr+OBRJUlgkNJEo78P4b0yRw= sha512-nNo+yCHEyn0smMxSswnf/OnX6/KwJuZTlNZBjauKhTK0c+zT+q5JOCx0UFhXQ6rJR9jg6Es8gPuD2uZcYDLqSw==" crossorigin="anonymous">
@stop

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Register</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">

                    	 {{ csrf_field() }}
                        <fieldset>

                            <div class="form-group">
                                <label class="col-md-4 control-label" for="Name (Full name)">Name (Full name)</label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-user"></i>
                                        </div>
                                        <input id="Name (Full name)" name="Name (Full name)" type="text" placeholder="Name (Full name)" class="form-control input-md">
                                    </div>


                                </div>


                            </div>

                            <!-- Profile Photo -->
                            <!-- <div class="form-group">
                                <label class="col-md-4 control-label" for="Upload photo">Upload photo</label>
                                <div class="col-md-4">
                                    <input id="Upload photo" name="Upload photo" class="input-file" type="file">
                                </div>
                            </div>
 -->
                            <!-- Date of Birth -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="Date Of Birth">Date Of Birth</label>
                                <div class="col-md-4">

                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-birthday-cake"></i>

                                        </div>
                                        <input id="Date Of Birth" name="Date Of Birth" type="text" placeholder="Date Of Birth" class="form-control input-md">
                                    </div>


                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label col-xs-12" for="Home Address">Home Address</label>
                                <div class="col-md-2  col-xs-4">
                                    <input id="Home Address" name="Home Address" type="text" placeholder="City" class="form-control input-md ">
                                </div>

                                <div class="col-md-2 col-xs-4">

                                    <input id="Home Address" name="Home Address" type="text" placeholder="Country" class="form-control input-md ">
                                </div>
                            </div>

							<!-- Email Address -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="Email Address">Email Address</label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-envelope-o"></i>

                                        </div>
                                        <input id="Email Address" name="Email Address" type="text" placeholder="Email Address" class="form-control input-md">

                                    </div>

                                </div>
                            </div>

                            <!-- Joined Date -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="Joined Date">Joined Date</label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-clock-o"></i>

                                        </div>
                                        <input id="Joined Date" name="Joined Date" type="text" placeholder="Joined Date" class="form-control input-md">

                                    </div>


                                </div>
                            </div>

                            <!-- Phone Number -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="Phone number ">Phone number </label>
                                <div class="col-md-4">
                                    
                                    <div class="input-group othertop">
                                        <div class="input-group-addon">
                                            <i class="fa fa-mobile fa-1x" style="font-size: 20px;"></i>

                                        </div>
                                        <input id="Phone number " name="Phone number " type="text" placeholder=" Phone number " class="form-control input-md">

                                    </div>

                                </div>
                            </div>

                            
                            <!-- Favorite Artists -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="Favorite Artists">Favorite Artists </label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-graduation-cap"></i>

                                        </div>
                                        <input id="Favorite Artists" name="Favorite Artists" type="text" placeholder="Favorite Artists" class="form-control input-md">
                                    </div>

                                </div>
                            </div>                                                                                                             
                            <!-- Multiple Checkboxes for Genre Intrested -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="Genre Intrested">Genre Intrested</label>
                                <div class="col-md-4">
                                    <div class="checkbox">
                                        <label for="Genre Intrested-0">
      <input type="checkbox" name="Genre Intrested" id="Genre Intrested-0" value="1">
      Rock
    </label>
                                    </div>
                                    <div class="checkbox">
                                        <label for="Genre Intrested-1">
      <input type="checkbox" name="Genre Intrested" id="Genre Intrested-1" value="2">
      Pop
    </label>
                                    </div>
                                    <div class="checkbox">
                                        <label for="Genre Intrested-2">
      <input type="checkbox" name="Genre Intrested" id="Genre Intrested-2" value="3">
      Jazz
    </label>
                                    </div>
                                    <div class="checkbox">
                                        <label for="Genre Intrested-3">
      <input type="checkbox" name="Genre Intrested" id="Genre Intrested-3" value="4">
      Hip Pop
    </label>
                                    </div>

                                  
                                </div>
                            </div>

                            
                            <!-- Textarea -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="Bio (max 200 words)">Bio (max 200 words)</label>
                                <div class="col-md-4">
                                    <textarea class="form-control" rows="10" id="Bio (max 200 words)" name="Bio (max 200 words)"></textarea>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-md-4 control-label"></label>
                                <div class="col-md-4">
                                    <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-thumbs-up"></span> Submit</a>
                                    <button type="reset" class="btn btn-danger" value=""><span class="glyphicon glyphicon-remove-sign"></span> Clear</a>

                                </div>
                            </div>

                        </fieldset>
                    </form>
                </div>


            </div>
        </div>
        <!-- jQuery Version 1.11.1 -->
        <script src="js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>
    </div>
</div>
</div>
</div>
</div>
@endsection