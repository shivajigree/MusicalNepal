<p align="center"><h1>MUSICAL NEPAL</h1></p>


## About Musical Nepal

Musical Nepal is a web application for Nepali music lovers who need a platform to showcase their skills and talents in the music field to a wide range of people online living anywhere. It is done as a Major Project for the completion of Bachelors of Engineering in Software Engineering by batch of 2014. The basic features of this application are:

- Only verified person can go live thereby reducing any hermful and illegal activites through video streaming
- Instant reporting can help block any potential bluffer
- Realtime video broadcasting
- User interactions with like, comments, and emojis
- Rewards system for both performers and viewers

Musical Nepal is accessible thorugh both mobile and web. But the performer needs a web application to go live.

## Using Musical Nepal

Musical Nepal has the most extensive and thorough documentation to get started using it. If you're not in the mood to perform, then simply view through a list of receommended artists according to your interest. Boost the skill level of yourself and your entire team by promoting the music industry.


## Contributions

Thank you for every faculties of GCES for their precious feedbacks. And our supervisor for the continuous support from start to the end of this project.

## Security Vulnerabilities

The website still is vulnerable to illegal acts in the streaming and we are working in controlling it to higher level.

## License

This is a college major project so Musical Nepal is definately an open source project and would like anybody to have their contribution in it.