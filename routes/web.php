<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/stream', 'PagesController@stream');

Route::get('/subscribe', 'PagesController@subscribe');

Route::get('/profile', 'PagesController@profile');

Route::get('/live', 'PagesController@live');

Route::get('/help', 'PagesController@help');

Route::get('/settings', 'PagesController@settings');

Route::get('/alertBox', function () {
    return view('eventListener');
});

Route::get('fireEvent', function () {
    event(new App\Events\eventTrigger());
});
